package com.example.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import model.User;
import service.UserService;

@SpringBootApplication
public class TestApplication {
	//@Autowired
	//static UserService userService;

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
		
		//User chanaz = new User("chanaz","123");
		//userService.AddUser(chanaz);
	}

}
