package repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Long> {

}
