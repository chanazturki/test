package config;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsLegacy implements UserDetailsService {
	@Autowired
	private PasswordEncoder passwordEncoder; 
	
	@Override
	public UserDetails loadUserByUsername(String username) {
		System.out.println("===============================================");
		List<String> s = new ArrayList<>();
		s.add("USER");
		return new User("testuser", passwordEncoder.encode("testpassword2"),
				s.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
	}


}
