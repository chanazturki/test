package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import model.Subject;
import repositories.SubjectRepository;

@RestController
public class SubjectController {
	
	@Autowired
	SubjectRepository subjectRepository;
	
	 @PostMapping(value="/subject/add")
		public void createSubject(@RequestBody Subject subject)
		
		{
		    subject.setTitle(subject.getTitle());
			subject.setDescription(subject.getDescription());
			subject.setVote(subject.getVote());

			subjectRepository.save(subject);
		}
	 
	 @PostMapping(value="/subject/update/{id}")
	 public void updateSubject(@RequestBody Subject subject , @PathVariable Long id)
	 {
	          Subject subjectFinded = subjectRepository.findById(id).orElseThrow(IllegalArgumentException::new);
	          subjectFinded.setVote(subjectFinded.getVote());
	          subjectRepository.save(subjectFinded);
	 }
	 
	 @GetMapping (value = "/subject/all")
	 public List<Subject> getAllSubjects()
	 {
		 return	subjectRepository.findAll() ;
         
	 }	
	 
	 @GetMapping(value = "/subject/find/{id}")
	 public Subject findSubject(@PathVariable Long id)	
	 { 

		 return	subjectRepository.findById(id).orElseThrow(IllegalArgumentException::new);
	 
	 }
}
