package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import model.User;
import repositories.UserRepository;


@RestController
public class UserController {

	@Autowired
	UserRepository userRepository;
	
	 @GetMapping(value = "/auth/all")
	 public List<User> getAllUsers()
	 {
		 return	userRepository.findAll() ;
         
	 }	
	 
	 @GetMapping(value = "/auth/find/{id}")
	 public User findUser(@PathVariable Long id)	
	 { 

		 return	userRepository.findById(id).orElseThrow(IllegalArgumentException::new);
	 
	 }
	 
	 @PostMapping(value="/user/add")
		public void createUser(@RequestBody User user)
		
		{
		    user.setUsername(user.getUsername());
			user.setPassword(user.getPassword());

			userRepository.save(user);
		}
		
}
